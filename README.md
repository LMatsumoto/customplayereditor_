Instantiate a character prefab in the scene view using a customized editor window, and edit the character fields using a customized inspector.

To instantiate a character prefab in the Scene...

Navigate to My Tools > Show Instantiate Character Window. This will open the window Instantiate Character In Scene. Fill in the necessary fields and instantiate the character in the scene.

To generate a scripted Player Data asset...

Option 1: (Previous to Unity 5.1) Select the Data Objects folder (if no such folder exists, create one) in the Project hierarchy. Then, navigate to Assets > Create > Create Player Scriptable Object in the menu bar. This will generate a scriptable Player Data object in the Data Objects folder.

Option 1: (Unity 5.1 and beyond) Navigate to the Data Objects folder in the Project hierarchy. Then, navigate to Assets > Create > My Content > Create Player Scriptable Object. This will enerate a scriptable asset in the Data Objects folder.