﻿// using System.Collections;
// using System.Collections.Generic;
using UnityEngine;

/* Create a scriptable object...
 * fileName: name of asset file that will be created.
 * menuName: menu hierarchy w/n Asset/Create menu
 * order: order of menu item in the Asset/Create menu. */
[CreateAssetMenu(fileName = "My Scriptable Object", menuName = "My Content/Create Player Scriptable Object", order = 0)]
public class PlayerData : ScriptableObject {

	/* GOAL OF THIS CLASS:
	 * Scriptable object holding player data. */

	public int m_health;		// Health of character
	public int m_damage;		// Damage character is able to inflict
	public int m_jumpForce;		// Jump force of character
	public int m_walkSpeed;		// Walk speed
	public int m_runSpeed;		// Run speed
}
