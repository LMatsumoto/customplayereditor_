﻿using UnityEngine;
using UnityEditor;

public class MyEditorUtils {

	/* GOAL OF THIS CLASS:
	 * Allow the user to save a Scriptable Object as an asset.
	 * Accessible through Menu: Assets > Create > Create Player Data Object */

	// Wrap in static method so it can be triggered through a menu item attribute action
	[MenuItem("Assets/Create/Create Player Scriptable Object")]
	public static void CreateAsset() {
		CreateAsset<PlayerData> ();
	}

	// Generic CreateAsset() method for scriptable objects only
	public static void CreateAsset<T>() where T : ScriptableObject {
		// Instantiate the object passed in as a parameter
		T asset = ScriptableObject.CreateInstance<T> ();

		// Get path of selected folder, Construct path/name string for the asset
		string path = AssetDatabase.GetAssetPath (Selection.activeObject);
		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

		/* Create a '.asset' file with specified name, path
		 * Save the asset and refresh the asset database.
		 * Focus the project window and select the saved asset. */
		AssetDatabase.CreateAsset (asset, assetPathAndName);
		AssetDatabase.SaveAssets ();
		AssetDatabase.Refresh ();
		EditorUtility.FocusProjectWindow ();
		Selection.activeObject = asset;
	}
}
