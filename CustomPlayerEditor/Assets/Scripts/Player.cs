﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Obtains information from specified PlayerData scripted object.
	 * Assigns information to attached Player object. */

	public PlayerData data;				// Reference the PlayerData scripted object

	public int _health;					// Player data information
	public int _damage;
	public int _jumpForce;
	public int _walkSpeed;
	public int _runSpeed;

	private MeshRenderer _renderer;		// MeshRenderer for the GameObject

	// Obtain data from the PlayerData Object
	void Awake() {
		Debug.Log ("PLAYER: Awake");
		if (data != null) {
			Debug.Log ("PLAYER: Data assigned.");
		} else {
			Debug.Log ("PLAYER: No data assigned.");
		}

		data = GetComponent<PlayerData> ();
		_renderer = GetComponent<MeshRenderer> ();
		_health = data.m_health;
		_damage = data.m_damage;
		_walkSpeed = data.m_walkSpeed;
		_runSpeed = data.m_runSpeed;
		_jumpForce = data.m_jumpForce;
	} 
}
