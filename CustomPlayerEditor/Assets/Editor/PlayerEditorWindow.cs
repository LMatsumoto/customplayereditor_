﻿using UnityEditor;
using UnityEngine;

public class PlayerEditorWindow : EditorWindow {

	/* GOAL OF THIS CLASS:
	 * Allow user to instantiate prefab game object in the scene.
	 * Accessible through My Tools > Show Instantiate Character Window */

	private static PlayerEditorWindow _winInstance;		// Instance of custom editor window
	public GameObject myGameObject;						// GameObject reference assigned in window
	public PlayerData myPlayerData;						// Reference to PlayerData object

	private bool gameObjectSelected;					// Determines if a prefab has been selected. Controls if user can instantiate an object in the scene.
	// private bool playerDataSelected;					// Determines if scriptable data object has been selected.

	// Set the "window path" so that it will show in the menu bar
	[MenuItem("My Tools/Show Instantiate Character Window")]
	public static void ShowWindow() {
		/* GetWindow(bool isUtility, String title) gets an instance of the specified type of window.
		 * Every time GetWindow() is called, the current live window instance is returned. */
		_winInstance = GetWindow<PlayerEditorWindow> (true, "Instantiate Character In Scene");
	}

	// Build GUI
	private void OnGUI() {
		// Everything between Begin and End Vertical() will be in the same window
		EditorGUILayout.BeginVertical();

		// Attach a Character Prefab
		EditorGUI.BeginChangeCheck();
		GUIContent goTip = new GUIContent ("Character Prefab", "Choose a prefab to instantiate by clicking the selector tool on the right side of the field box.");
		myGameObject = EditorGUILayout.ObjectField(goTip, myGameObject, typeof(GameObject), true) as GameObject;

		// If a prefab has been selected... set 'gameObjectSelected' to true.
		if (EditorGUI.EndChangeCheck ()) {
			if (myGameObject != null) {
				gameObjectSelected = true;
				Debug.Log ("Game object selected.");
			} else {
				gameObjectSelected = false;
				Debug.Log ("No game object has been selected.");
			}
		}
			
		// TODO: Find a way to instatiate the game object with the attached Scriptable Object?
		// If the User should assign a data object before instantiating an object, uncomment the following code 
		// (and add 'playerDataSelected' to the if statement below).

		// Attach a serializable PlayerData object
		/* EditorGUI.BeginChangeCheck();
		GUIContent pdTip = new GUIContent("Player Data", "Attach a scriptable PlayerData object to the selected prefab. This is not necessary to instantiate the character object, but is recommended for persistent data purposes.");
		myPlayerData = EditorGUILayout.ObjectField (pdTip, myPlayerData, typeof(PlayerData), true) as PlayerData;

		// If a PlayerData object has been selected... set 'playerDataSelected' to true.
		if (EditorGUI.EndChangeCheck ()) {
			if (myPlayerData != null) {
				playerDataSelected = true;
				Debug.Log ("Player data object selected.");
			} else {
				playerDataSelected = false;
				Debug.Log ("No player data object has been selected.");
			}
		}

		// Info box for users. It is not required to attach a data object, but recommended.
		EditorGUILayout.HelpBox ("For persistent data purposes, it is recommended to attach a serializable data object to the game object.", MessageType.Info); */

		// User is only allowed to instantiate a character (i.e. the button is only enabled) if a game object has been selected
		GUI.enabled = false;
		if (gameObjectSelected) { 
			GUI.enabled = true;
		}

		// Once you have the prefab... you can Instantiate it in the scene.
		if (GUILayout.Button ("Instantiate Character Prefab in Scene")) {
			Instantiate (myGameObject);
		}

		EditorGUILayout.EndVertical ();
	}
}
