﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Player))]
public class PlayerEditor : Editor {

	/* GOAL OF THIS CLASS:
	 * Generate a custom Inspector for Player objects.
	 * Accessible when Player object is selected in hierarchy. */

	private Player m_target;			// Reference to Player object
	private int maxInt = 5000;			// Maximum integer allowed

	// Called when Player GameObject is selected
	void OnEnable() {
		Debug.Log ("OnEnable is called");
		m_target = (Player)target;
	}

	// Called when Player GameObject is deselected
	void OnDisable() {
		Debug.Log ("OnDisable is called");
	}

	// Called when custom inspector is destroyed
	void OnDestroy() {
		Debug.Log ("OnDestroy is called");
	}

	// Gather Player information from attached game object and display in the Editor window
	public override void OnInspectorGUI() {
		GUIContent pdTip = new GUIContent("Player Data", "Attach a scriptable PlayerData object to the selected prefab. Recommended for persistent data purposes.");
		m_target.data = EditorGUILayout.ObjectField (pdTip, m_target.data, typeof(PlayerData), true) as PlayerData;

		GUIContent healthTip = new GUIContent("Health", "Controls the current health of the character. [ 0, " + maxInt.ToString() + " ].");
		m_target._health = EditorGUILayout.IntField (healthTip, Mathf.Clamp(m_target._health, 0, maxInt));

		GUIContent damageTip = new GUIContent ("Damage", "Controls the amount of damage the character can inflict. Upon inflicting damage on another character, that character's 'health' will decrease by this amount. [ 0, " + maxInt.ToString() + " ].");
		m_target._damage = EditorGUILayout.IntField (damageTip, Mathf.Clamp(m_target._damage, 0, maxInt));

		GUIContent jumpTip = new GUIContent ("Jump Force", "Control the force behind a character's jump. This force will be applied as vertical force to the GameObject's rigidbody. [ 0, " + maxInt.ToString() + " ].");
		m_target._jumpForce = EditorGUILayout.IntField (jumpTip, Mathf.Clamp(m_target._jumpForce, 0, maxInt));

		GUIContent walkTip = new GUIContent ("Walk Speed", "Control the speed of the character's walk. This will be applied as horizontal force to the GameObject's rigidbody. [ 0, " + maxInt.ToString() + " ].");
		m_target._walkSpeed = EditorGUILayout.IntField (walkTip, Mathf.Clamp(m_target._walkSpeed, 0, maxInt));

		GUIContent runTip = new GUIContent ("Run Speed", "Control the speed of the character's run. This will be applied as horizontal force to the GameObject's rigidbody. [ 0, " + maxInt.ToString() + " ].");
		m_target._runSpeed = EditorGUILayout.IntField (runTip, Mathf.Clamp(m_target._runSpeed, 0, maxInt));
	}
}
